# License

tldr: Use the code and assets developed for this game however you want. It
would be nice if you link back to my pages and projects to share the love.

External assets you'll have to provide attribution

NVIDIA flex packages under Content/TestPackages/Flex are their code and
you'll have to refer to their docs


## Code
See LICENSE/MIT.txt

## Assets
Assets developed by me for this jam: see LICENSE/CC-BY-4.0.txt

Third party assets such as the song "Salt Mines" by Meizong:
- see LICENSE/CC-BY-3.0.txt
- Their music can be found here https://soundcloud.com/dj-meizong/salt-mines

## NVIDIA FLEX
NVIDIA FLEX assets are likely available under the NVIDIA Source Code License
referenced at:
https://github.com/NVIDIAGameWorks/FleX/blob/master/LICENSE.txt

or they may fall under the Unreal Engine EULA because they are a distributed
with a custom build of the Engine

Information on both of these in the ./LICENSE folder
